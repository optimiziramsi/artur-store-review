# LINUX TRICKS
## PERMISSION DENIED ON FILES / FOLDERS
- add current user to group "www-data"
- CMD:
    sudo usermod -a -G www-data oranzo
- change "wordpress" folder permissions
- CMD:
    sudo chmod 775 -R wordpress/

- that should be it, happy developing :)

## GENERATE POT
1) go to fulder, where you want your .pot file to be generated
2) generate pot file with command
example:
    php path/to/makepot.php wp-theme path/to/your-theme-directory
    php path/to/makepot.php wp-plugin path/to/your-plugin-directory
CMD:
    php /home/oranzo/JOB/wp-dev/develop.svn.wordpress.org/trunk/tools/i18n/makepot.php wp-plugin /home/oranzo/JOB/projects/git/artur-store-review/artur-store-reviews